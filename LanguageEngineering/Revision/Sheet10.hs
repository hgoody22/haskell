module Sheet10 where

data Nondet cnt = Fail
                | Split cnt cnt

data Free f k = Var k
              | Con (f (Free f k))

instance Functor f => Functor (Free f) where
  -- fmap :: (a -> b) -> Free f a -> Free f b
  fmap f (Var x)  = Var (f x)
  fmap f (Con op) = Con (fmap (fmap f) op)

-- instance Functor f => Applicative (Free f) where
--   -- pure :: a -> Free f a
--   pure x = Var x
--   -- (<*>) :: Free f (a -> b) -> Free f a -> Free f b

instance Functor f => Monad (Free f) where
  -- return :: a -> Free f a
  return x = Var x
  -- (>>=) :: Free f a -> (a -> Free f b) -> Free f b
  (Var x)  >>= f = f x
  (Con op) >>= f = Con(fmap (>>= f) op)


instance Functor Nondet where
  -- fmap :: (a -> b) -> Nondet a -> Nondet b
  fmap f Fail = Fail
  fmap f (Split one two) = Split (f one) (f two)

data Exception' e cnt = Throw e
                      | Continue cnt

instance Functor (Exception' e) where
  -- fmap :: (a -> b) -> Exception' e a -> Exception' e b
  fmap f (Throw y)    = Throw y
  fmap f (Continue x) = Continue (f x)

data STATE s cnt = Get (s -> cnt)
                 | Put s cnt

instance Functor (STATE s) where
  -- fmap :: (a -> b) -> STATE s a -> STATE s b
  fmap f (Get x)   = Get (f . x)
  fmap f (Put s y) = Put s (f y)


eval :: Functor f => (f a -> a) -> Free f a -> a
eval alg (Var x)  = x
eval alg (Con op) = alg (fmap (eval alg) op)

handle :: Functor f => (a -> b) -> (f b -> b) -> Free f a -> b
handle gen alg = eval alg . fmap gen

handleNondet :: Free (Nondet) a -> [a]
handleNondet = handle gen alg where
    gen :: a -> [a]
    gen x = [x]
    alg :: Nondet [a] -> [a]
    alg Fail          = []
    alg (Split xs ys) = xs ++ ys
























-- end
