module Sheet7 where

import Yoda
import Data.Monoid

data Chess = Turn Move Move Chess
           | EndGame
           deriving (Show)

data Move = Move MoveP Quant
          | Cslt Bool
          | End Winner
          deriving (Show)

data Quant = Prom Piece Quant
           | Chck Quant
           | Null
           deriving Show

data MoveP = Alg Piece Cell
           | Smh Cell Cell
           | AlgDis Piece Cell Cell
           | Tke Piece Cell
           deriving (Show)

data Winner = White
            | Black
            | Draw
            | AO
            deriving (Show)

data Cell = Cel Char Int
    deriving (Show)

data Piece = King
           | Queen
           | Rook
           | Knight
           | Bishop
           | Pawn
           deriving (Show, Eq)








































--end
