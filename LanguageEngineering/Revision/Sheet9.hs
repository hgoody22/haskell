module Sheet9 where

data State s a = State (s -> (s, a))

runState :: State s a -> s -> (s, a)
runState (State prog) = prog

execState :: State s a -> s -> s
execState prog = \s -> fst(runState prog s)

evalState :: State s a -> s -> a
evalState prog = \s -> snd (runState prog s)

get :: State s s
get = State (\s -> (s, s))

put :: s -> State s ()
put x = State (\_ -> (x, ()))

instance Functor (State s) where
  -- fmap :: (a -> b) -> State s a -> State s b
  fmap f state = state >>= (return . f)

instance Applicative (State s) where
  -- pure :: a -> State s a
  pure x = State(\s -> (s, x))
  -- (<*>) :: State s (a -> b) -> State s a -> State s b
  State f <*> State x = State (\s -> let (s', f') = f s
                                         (s'', x') = x s'
                                         in (s'', f' x'))

instance Monad (State s) where
  -- return :: a -> State s a
  return x = State (\s -> (s, x))
  -- (>>=) :: State s a -> (a -> State s b) -> State s b
  State x >>= f = State (\s -> let (s', y) = x s in runState (f y) s')

add :: Int -> Int -> State Int Int
add x y = State (\s -> (s + x, x + y))

add' :: Int -> Int -> State Int Int
add' x y = do
  s <- get
  put (s + x)
  return (s + x + y)

multiply :: Int -> Int -> State Int Int
multiply x y = State (\s -> (s + y, s * y * x))

multiply' :: Int -> Int -> State Int Int
multiply' x y = do
  s <- get
  put (s + y)
  return (s * s * y)

sub :: Int -> Int -> State Int Int
sub x y = State (\s -> (s - 1, s - x + y))

add3multiply2 :: Int -> State Int Int
add3multiply2 x = add 3 x >>= multiply 2

runOp :: Int -> Int -> (Int, Int)
runOp x s = runState (add3multiply2 x) s


type MiniImp = State (Int, Int)

getX :: MiniImp Int
getX = fst <$> get

getY :: MiniImp Int
getY = snd <$> get

setX :: Int -> MiniImp ()
setX x = do (_, y) <- get
            put (x, y)

setY :: Int -> MiniImp ()
setY y = do (x, _) <- get
            put (x, y)

modify :: (s -> s) -> State s ()
modify f = State (\s -> (f s, ()))














-- end
