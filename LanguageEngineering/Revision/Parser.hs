module Parser where
import Control.Monad
import Control.Applicative

-- Our parsers will take in a `String` and produce a list of possible
-- parses, along with remaining unparsed strings.

newtype Parser a = Parser ( String -> [(String, a)] )

parse :: Parser a -> String -> [(String, a)]
parse ( Parser px ) ts = px ts

-- produce :: a -> String -> [(String, a)]
-- produce x ts = [(ts, x)]

--Parser version of produce
produce :: a -> Parser a
produce x = Parser ( \ts -> [ (ts, x) ] )

fail :: Parser a
fail = Parser (\ts -> [])

--Tells you what next item is
item :: Parser Char
item = Parser( \ts -> case ts of
  []     -> []
  (t:ts) -> [(ts, t)] )

instance Functor Parser where
--fmap :: (a -> b) -> Parser a -> Parser b
    fmap f (Parser px) =
        Parser (\ts -> [ (ts', f x) | (ts', x) <- px ts ] )
                         -- ts'   :: String
                         -- x     :: a
                         -- px ts :: [(String, a)]

-- Just fmap
-- (<$>) :: Functor f => (a -> b) -> f a -> f b
-- f <$> x = fmap f x
--
-- -- To parse and ignore function
-- (<$) :: Functor f => a -> f b -> f a
-- (<$) = fmap . const

---------------------------------------------------------------------
-- Applicative ------------------------------------------------------
---------------------------------------------------------------------

instance Applicative Parser where
-- pure :: a -> Parser a
   pure x = Parser (\ts -> [(ts, x)])

-- (<*>) :: Parser (a -> b) -> Parser a -> Parser b
   Parser pf <*> Parser px = Parser (\ts ->
     [(ts'', f x) | (ts', f) <- pf ts, (ts'', x) <- px ts'] )
     -- f :: a -> b
     -- x :: a

(<*) :: Applicative f => f a -> f b -> f a
px <* py = const <$> px <*> py

(*>) :: Applicative f => f a -> f b -> f b
px *> py = flip const <$> px <*> py


---------------------------------------------------------------------
-- Alternative ------------------------------------------------------
---------------------------------------------------------------------

instance Alternative Parser where
-- empty :: Parser a
   empty = Parser (\ts -> [])

-- (<|>) :: Parser a -> Parser a -> Parser a
   Parser px <|> Parser py = Parser (\ts -> px ts ++ py ts)

(<:>) :: Applicative f => f a -> f [a] -> f [a]
x <:> xs = (:) <$> x <*> xs

some :: Alternative f => f a -> f [a]
some px = px <:> many px

-- many :: Alternative f => f a -> f [a]
-- many px = some px <|> empty -- Either some px or nothing

---------------------------------------------------------------------
-- Tree example ------------------------------------------------------
---------------------------------------------------------------------

data Tree a = Leaf a
            | Branch [(Tree a)]
    deriving (Show)

instance Functor Tree where
    fmap f (Leaf a)    = Leaf (f a)
    fmap f (Branch as) = Branch (map (fmap f) as)

-- (<$) :: a -> Tree b -> Tree a
-- a <$ (Leaf b)    = Leaf a
-- a <$ (Branch as) = Branch (map (a <$) as)

instance Applicative Tree where
-- pure :: a -> f a
   pure x = Leaf x
-- (<*>) :: f (a -> b) -> f a -> f b
   (Leaf f)    <*> (Leaf x)    = Leaf (f x)
   (Branch fs) <*> (Leaf x)    = Branch ( [ f <*> (Leaf x) | f <- fs ] )
   (Leaf f)    <*> (Branch as) = Branch (map (fmap f) as)
   (Branch fs) <*> (Branch as) = Branch ([ f <*> a | a <- as, f <- fs])

instance Alternative Tree where
-- empty :: Tree a
   empty = Branch []
-- (<|>) :: Tree a -> Tree a -> Tree a
   (Branch xs) <|> (Branch ys) = Branch (xs ++ ys)
   (Branch xs) <|> b           = Branch (xs ++ [b])
   b           <|> (Branch ys) = Branch ([b] ++ ys)
   a           <|> b           = Branch [a, b]

-- instance Alternative Maybe where
-- -- empty :: Maybe a
--    empty = Nothing
-- -- (<|>) :: Tree a -> Tree a -> Tree a
--    (Just x) <|> _ = Just x
--    Nothing <|> y = y
--


--end
