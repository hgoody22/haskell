module Algebra where

------------------------------------------------------------
-- Algebra for length of list ------------------------------
------------------------------------------------------------

data ListF a k = EmptyF
               | ConsF a k
               deriving (Show)

instance Functor (ListF a) where
    -- fmap :: ( a -> b ) -> ( f a -> f b )
    fmap f EmptyF = EmptyF
    fmap f ( ConsF a k ) = ConsF a ( f k )

data Fix f = In ( f ( Fix f ) )


-- alg :: ListF a b -> b
-- alg EmptyF = k
-- alg ( ConsF x y ) = f x y

cata :: Functor f => (f b -> b) -> Fix f -> b
cata alg (In x) = (alg . fmap ( cata alg ) ) x

lengthF :: Fix ( ListF a ) -> Int
lengthF = cata alg
      where alg :: ListF a Int -> Int
            alg EmptyF        = 0
            alg ( ConsF a x ) = 1 + x

sumAlg :: Fix ( ListF Int ) -> Int
sumAlg = cata alg
     where alg :: ListF Int Int -> Int
           alg EmptyF        = 0
           alg ( ConsF a k ) = a + k

toListF :: [a] -> Fix (ListF a)
toListF = foldr f k where
    f :: a -> Fix (ListF a) -> Fix (ListF a)
    f x xs = In (ConsF x xs)
    k :: Fix (ListF a)
    k = In EmptyF

fromListF :: Fix (ListF a) -> [a]
fromListF = cata alg where
    alg :: ListF a [a] -> [a]
    alg EmptyF = []
    alg (ConsF x xs) = x:xs

data NatF k = ZF | SF k deriving (Show)

instance Functor NatF where
    fmap f ZF     = ZF
    fmap f (SF k) = SF (f k)

toInt :: Fix NatF -> Int
toInt = cata alg where
    alg :: NatF Int -> Int
    alg ZF     = 0
    alg (SF k) = 1 + k















-- end
