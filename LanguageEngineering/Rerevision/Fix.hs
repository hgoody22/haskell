module Fix where

--factorial
fac :: Int -> Int
fac n | n < 0     = error ("positive numbers only you cunt!")
      | n == 0    = 1
      | otherwise = n * fac (n - 1)

fix :: (a -> a) -> a
fix f = f (fix f)

fac' :: Int -> Int
fac' n = fix (\f n -> if n == 0 then 1 else n * f (n - 1)) n
