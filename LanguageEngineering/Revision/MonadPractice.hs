module MonadPractice where

newtype Seq a = Seq a

instance Functor Seq where
-- fmap :: (a -> b) -> Seq a -> Seq b
   fmap f (Seq x) = Seq (f x)

instance Applicative Seq where
-- pure :: a -> Seq a
   pure = Seq

-- (<*>) :: Seq (a -> b) -> Seq a -> Seq b
   Seq f <*> Seq x = Seq (f x)

instance Monad Seq where
-- return :: a -> Seq a
   return = Seq

-- (>>=) :: Seq a -> (a -> Seq b) -> Seq b
   Seq x >>= f = f x

increment :: Int -> Int
increment x = x + 1

double :: Int -> Int
double x = x * 2

-- do notation is syntactic sugar for (>>=)
-- do s1; s2      = s1 >>= s2
-- do a <- s1; s2 = s1 >>= \a -> s2










































--
