module Tres where

data MaybeError a = Error String
                  | Jut a

instance Functor MaybeError where
-- fmap :: (a -> b) -> MaybeError a -> MaybeError b
   fmap f (Error string) = Error string
   fmap f (Jut x)        = Jut (f x)

-- Functor Laws --------------------------------------------------------
-- fmap id = id
--
-- fmap id (Error string)
-- {def fmap}
-- = Error string
-- {def id}
-- id (Error string)
--
-- fmap id (Just x)
-- {}
--
--fmap id (Just x) = Just (id)
--
-- fmap f . fmap g = fmap (f . g)


data Tree a = Leaf
            | Branch a (Tree a) (Tree a)

instance Functor Tree where
-- fmap :: (a -> b) -> Tree a -> Tree b
   fmap f Leaf                  = Leaf
   fmap f (Branch x left right) = Branch (f x) (fmap f left) (fmap f right)

---------------------------------------------------------------------------
-- Fixpoints
---------------------------------------------------------------------------

data Fix f = In (f (Fix f))

inOp :: Fix f -> f (Fix f)
inOp (In x) = x

data TreeF a k = LeafF
               | BranchF a k k

instance Functor (TreeF a) where
-- fmap :: (p -> q) -> TreeF a p -> TreeF a q
   fmap f LeafF = LeafF
   fmap f (BranchF x l r) = BranchF x (f l) (f r)

cata :: Functor f => (f b -> b) -> Fix f -> b
cata alg = alg . fmap (cata alg) . inOp

sumTree :: Num a => Tree a -> a
sumTree Leaf = 0
sumTree (Branch x l r) = x + (sumTree l) + (sumTree r)

sumTreeF :: Fix (TreeF Int) -> Int
sumTreeF = cata alg where
    -- alg :: TreeF a Int -> Int
    alg LeafF           = 0
    alg (BranchF x l r) = x + l + r

countLeaves :: Tree a -> Int
countLeaves Leaf = 1
countLeaves (Branch x l r) = 1 + (countLeaves l) + (countLeaves r)

countFix :: Fix (TreeF a) -> Int
countFix = cata alg where
   -- alg :: TreeF a Int = Int
   alg LeafF = 1
   alg (BranchF0 x l r) = 1 + l + r




















-- end
