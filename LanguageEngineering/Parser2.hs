module Parser2 where
import Prelude
import Yoda
import Data.Monoid

--1.1.1 Applicative of Maybe

data Mayb a = Nothin | Jut a

instance Functor Mayb where
  --fmap :: (a -> b) -> f a -> f b
  fmap f Nothin = Nothin
  fmap f (Jut a) = Jut (f a)

instance Applicative Mayb where
  -- pure :: a -> f a
  pure a = Jut a
  -- (<*>) :: f (a -> b) -> f a -> f b
  Nothin <*> _ = Nothin
  _ <*> Nothin = Nothin
  Jut f <*> Jut a = Jut (f a)

--1.1.2 Applicative Laws

-- (a) pure id <*> v = v
--     pure id <*> Nothing = Nothing
--     pure id <*> Jut a = Jut (id a) = Jut a

-- (b)  pure f <*> pure x = pure (f x)
--(LHS) pure f <*> pure x = Jut f <*> Jut x
--                       = Jut (f x)
--(RHS) pure (f x) = Jut (f x)
-- Therefore LHS == RHS

-- ($) :: (a -> b) -> a -> b
-- (c)  u <*> pure y = pure ($y) <*> u
--(LHS) u <*> pure y = u <*> Jut y
--(RHS) pure ($y) <*> u = Jut ($y) <*> u
--
-- The dollar is an operator (like '+' so is waiting for something to go
-- infront of it) and u is a parser of a function so that function goes in front
-- of the dollar

-- (d) pure ()

--1.2

data List a = Empty | Cons a (List a) deriving (Show)

instance Monoid (List a) where
  mempty = Empty
  Empty `mappend` y = y
  Cons x xs `mappend` ys = Cons x (xs <> ys)

--1.2.1
instance Functor List where
  --fmap :: (a -> b) -> f a -> f b
  fmap f Empty = Empty
  fmap f (Cons a xs) = Cons (f a) (fmap f xs)

--1.2.2
(<£) :: a -> List b -> List a
x <£ Empty = Empty
x <£ (Cons p ps) = Cons x (x <£ ps)

--1.2.3
instance Applicative List where
  -- pure :: a -> f a
  pure a = Cons a (Empty)
  -- (<*>) :: f (a -> b) -> f a -> f b
  _ <*> Empty = Empty
  Empty <*> _ = Empty
  (Cons f fs) <*> (Cons a as) = Cons (f a)
                                (fs <*> as)





























--end
