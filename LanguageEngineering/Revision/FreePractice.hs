module FreePractice where

-- data Tree a = Leaf a
--             | Fork (Tree a) (Tree a)
--
-- data List a = End
--             | Cons (a) (List a)
--
-- data TreeAbstract a = Leaf a
--                     | Fork (List (TreeAbstract a))
--
-- data TreeAbstract2 f a = Var a
--                        | Con (f (TreeAbstract2 f a))

data Free f a = Var a
              | Con (f (Free f a))

instance Functor f => Functor (Free f) where
  -- fmap :: (a -> b) -> Free f a -> Free f b
  fmap g (Var x) = Var (g x)
  fmap g (Con op) = Con (fmap (fmap g) op)
  -- where g :: a -> b
  --       op :: Functor f => f (Free f a)

instance Functor f => Monad (Free f) where
  -- return :: a -> Free f a
  return x = Var x
  -- (>>=) :: Free f a -> (a -> Free f b) -> Free f b
  (Var x)  >>= g = g x
  (Con op) >>= g = Con (fmap (>>= g) op)

data Nondet k = Stop | Or k k

instance Functor Nondet where
  --fmap :: (a -> b) -> Nondet a -> Nondet b
  fmap f Stop = Stop
  fmap f (Or x y) = Or (f x) (f y)


















-- end
