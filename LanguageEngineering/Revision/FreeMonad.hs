module FreeMonad where
import Data.Monoid
import Data.Functor
import Control.Applicative

data Free f a = Var a
              | Con (f (Free f a))

data Nondet k = Stop
              | Or k k

instance Functor f => Functor (Free f) where
-- fmap :: (a -> b) -> Free f a -> Free f b
   fmap f (Var v)  = Var (f v)
   fmap f (Con op) = Con (fmap (fmap f) op)

instance Functor f => Applicative (Free f) where
  --pure :: a -> Free f a
  pure x = Var x
  --(<*>) :: Free f (a -> b) -> Free f a -> Free f b
  (Var f) <*> free = fmap f free
  (Con fs) <*>

instance Functor f => Monad (Free f) where
  -- return :: a -> Free f a
  return x = Var x
  -- (>>=) :: Free f a -> (a -> Free f b) -> Free f b
  (Var v)  >>= f = f v
  (Con op) >>= f = Con (fmap (>>= f) op)





































--end
