module Sheet2 where

data Animal = Lion
            | Tiger
            | Gazelle
            | Ant
      deriving ( Eq, Show )

type Animals = [ Animal ]

-------------------------------------------------------------------------------
-- Shallow embedding -------------------------------------------------------------
-------------------------------------------------------------------------------

shallowTerritory :: Animals -> ( Int, Int )
shallowTerritory xs = ( 1, length xs )

shallowQuadrant :: ( Int, Int ) -> ( Int, Int ) -> ( Int, Int ) -> ( Int, Int ) -> ( Int, Int )
shallowQuadrant (a1, a2) (b1, b2) (c1, c2) (d1, d2) = (a1 + b1 + c1 + d1, maximum [a2, b2, c2, d2])

shallowTrid :: ( Int, Int , Animals) -> ( Int, Int, Animals ) -> ( Int, Int, Animals ) -> ( Int, Int, Animals )
shallowTrid (b1, b2, ws) (c1, c2, xs) (d1, d2, ys) = (b1 + c1 + d1, maximum [b2, c2, d2], ws ++ xs ++ ys)


-------------------------------------------------------------------------------
-- Deep embedding -------------------------------------------------------------
-------------------------------------------------------------------------------

data Safari = Quad Safari Safari Safari Safari
            | Territory Animals
            | Trid Safari Safari Safari

-- Count number of territories in Safari
countTerr :: Safari -> Int
countTerr (Territory xs) = 1
countTerr (Trid a b c) c = sum (map countTerr [a, b, c])
countTerr (Quad a b c d) = sum (map countTerr [a, b, c, d])

-- Calculate number of animals in territory with most Animals
countAnimals :: Safari -> Int
countAnimals (Territory xs) = length xs
countAnimals (Trid a b c)   = maximum (map countAnimals [a, b, c])
countAnimals (Quad a b c d) = maximum (map countAnimals [a, b, c, d])

-- List of all animals in Safari
totalZoo :: Safari -> Animals
totalZoo (Territory xs) = xs
totalZoo (Quad a b c d) = concat (map totalZoo [a, b, c, d])

-- Core data type
data Core = Plot [(Core, Float)]
          | Terr Animals

-- Number of territories in a core
coreTerr :: Core -> Int
coreTerr (Terr xs) = 1
coreTerr (Plot xs) = sum (map (coreTerr . fst) xs)

--number of animals in most populated territory of core
coreAnimals :: Core -> Int
coreAnimals (Terr xs) = length xs
coreAnimals (Plot xs) = maximum (map (coreAnimals . fst) xs)

--List of animals in core
coreZoo :: Core -> Animals
coreZoo (Terr xs) = xs
coreZoo (Plot xs) = concat (map (coreZoo . fst) xs)

-- Smart constructor
quad :: Core -> Core -> Core -> Core -> Core
quad a b c d = Plot [(a, 1/4), (b, 1/4), (c, 1/4), (d, 1/4)]

















--end
