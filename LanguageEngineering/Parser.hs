module Parser where
import Prelude
import Yoda

-- newtype Parser a = Parser { String -> [(String, a)] }
-- type Parser = Parsec () String

{-
Here is a grammar we might want to parse:

<expr> ::= <term> ("+" <term>)*
<term> ::= ('0' | '1' | ... | '9')+

-}


-- Parser a = Parser { String -> [(String, a)] }
-- parse :: String -> [(String, a)]

-- 1.1.1
-- a) string :: String -> Parser String
-- Turns String into a parser string

-- b) many :: f a -> f [a]
-- Returns 0 or more recursions of a

-- c) some :: f a -> f [a]
-- Returns 1 or more recursions of a

-- d) oneOf :: [Char] -> Parser Char
-- Parses from string the char's given as argument

-- e) noneOf :: [Char] -> Parser char
-- Exact opposite of oneOf

-- 1.1.2
whitespace :: Parser ()
whitespace = many (oneOf (" \t\n,.")) *> pure ()

-- 1.1.3
tok :: String -> Parser String
tok xs = whitespace *> string xs

-- 1.1.4
number :: Parser Int
number = whitespace *> (read <$> oneOf ("123456789") <:> many (oneOf ("0123456789")))

-- 1.2

robot :: Parser Robot
robot = Fwd    <$ tok "forward" <*> number     <*> robot
      <|> L    <$ tok "rotate"  <* tok "left"  <*> robot
      <|> R    <$ tok "rotate"  <* tok "right" <*> robot
      <|> Stop <$ tok "stop"


data Robot = Fwd Int Robot
             | L Robot
             | R Robot
             | Stop
             deriving (Show)

-- Tree data type
data Tree a = Leaf a
            | Branch [(Tree a)]
          deriving (Show)
-- Make it a functor instance
instance Functor Tree where
  -- fmap :: (a -> b) -> f a -> f b
  fmap f (Leaf a) = Leaf (f a)
  fmap f (Branch xs) = Branch (map (fmap f) xs)

-- <$ == <%
(<%) :: a -> Tree b -> Tree a
a <% (Leaf b) = Leaf a
a <% (Branch bs) = Branch (map (a <%) bs)

-- Make tree an instance of Applicative
instance Applicative Tree where
  -- pure :: a -> f a
  pure a = Leaf a
  -- (<*>) :: f (a -> b) -> f a -> f b
  (Leaf f) <*> (Leaf a) = Leaf (f a)
  (Branch fs) <*> (Leaf a) = Branch ([f <*> (Leaf a) | f <- fs])
  (Leaf f) <*> (Branch as) = fmap f (Branch as)
  (Branch fs) <*> (Branch as) = (Branch [f <*> x | f <- fs, x <- as])

-- <! == <*
(<!) :: Tree x -> Tree y -> Tree x
a <! (Leaf b) = a
(Leaf a) <! (Branch bs) = Branch (map (a <%) bs)
(Branch as) <! (Branch bs) = Branch ([a <! b | a <- as, b <- bs])

-- !> == *>
(!>) :: Tree x -> Tree y -> Tree y
(Leaf a) !> b = b
(Branch as) !> (Leaf b) = Branch (map (b <%) as)
(Branch as) !> (Branch bs) = Branch ([a !> b | a <- as, b <- bs])





















-- end
