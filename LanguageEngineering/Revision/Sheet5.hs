module Sheet5 where
import Yoda

-- 1.1.1
-- (a) string :: String -> Parser String
parseHelloWorld :: Parser String
parseHelloWorld = string "Hello World"

-- (b) many :: f a -> f [a]
parseStar :: Parser [String]
parseStar = many (string "0")

-- (c) some :: f a -> f [a]
parsePlus :: Parser [String]
parsePlus = some (string "1")

-- (d) oneOf :: [Char] -> Parser Char
parseBinary :: Parser String
parseBinary = many (oneOf ['0', '1'])

-- (e) noneOf :: [Char] -> Parser Char
parseNoEOL :: Parser String
parseNoEOL = many (noneOf ['\n'])

-- (f) try :: Parser a -> Parser a
parseTryBinary :: Parser String
parseTryBinary = try (parseBinary)

-- 1.1.2
-- Define a function whitespace :: Parser () which takes in whitespace (spaces, newlines, commas, full stops and tabs) and discards it.
whitespace :: Parser ()
whitespace = many (oneOf (" \t\n,.")) *> pure ()

-- 1.1.3 Define function tok :: String -> Parser String that tries to consume the string given,
-- and removes any surrounding whitespace. This will use the string function
tok :: String -> Parser String
tok ts = whitespace *> string ts

-- 1.1.4 Define number :: Parser Int to recognise numbers
number :: Parser Int
number = whitespace *> (read <$> oneOf ("123456789") <:> many (oneOf ("0123456789")))

numberM :: Parser Int
numberM = ( oneOf "123456789" <:> many ( oneOf "1234567890" ) ) >>= return . read

-- 1.2.1 Parse robot instructions
parseRobot :: Parser Robot
parseRobot = Fwd    <$ tok "forward" <*> number     <*> parseRobot
           <|> Lt   <$ tok "rotate"  <* tok "left"  <*> parseRobot
           <|> Rt   <$ tok "rotate"  <* tok "right" <*> parseRobot
           <|> Stop <$ tok "stop"





data Robot = Fwd Int Robot
           | Rt Robot
           | Lt Robot
           | Stop
           deriving Show

data Compass = N
             | E
             | S
             | W


--end
