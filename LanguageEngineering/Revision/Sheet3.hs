module Sheet3 where

data MaybeError a = Error String
                  | Jut a

instance Functor MaybeError where
--  fmap :: (a -> b) -> f a -> f b
    fmap f (Error string) = Error string
    fmap f (Jut a)       = Jut (f a)

-- Functor Law 1 :: fmap id = id
--   fmap id (Error string)
-- = {def fmap}
--   Error string
-- = {def id}
--   id (Error string)
--
--   fmap id (Jut a)
-- = {def fmap}
--   Jut (id a)
-- = {def id}
--   Jut a
-- = {def id}
--   id (Jut a)

-- Functor Law 2 :: fmap g . fmap f = fmap (g . f)
--   fmap g . fmap f (Error string)
-- = {def fmap}
--   fmap g (Error string)
-- = {def fmap}
--   Error string
-- = {def fmap}
--   fmap (g . f) (Error string)
--
--   fmap g . fmap f (Jut a)
-- = {def fmap}
--   fmap g (Jut (f a))
-- = {def fmap}
--   Jut (g (f a))
-- = {def .}
--   Jut ((g . f) a)
-- = {def fmap}
--   fmap (g . f) (Jut a)

data Tree a = Leaf
            | Branch a (Tree a) (Tree a)

instance Functor Tree where
  fmap f Leaf = Leaf
  fmap f (Branch x left right) = Branch (f x) (fmap f left) (fmap f right)

-- Kind (Tree) :: * -> *

-- Sum values of tree
sumTree :: Tree Int -> Int
sumTree Leaf = 0
sumTree (Branch x left right) = x + (sumTree left) + (sumTree right)

-- Count leaves on Tree
countLeaves :: Tree Int -> Int
countLeaves Leaf = 1
countLeaves (Branch x left right) = (countLeaves left) + (countLeaves right)

data Exception e a = Except e
                   | Result a

instance Functor (Exception e) where
    fmap f (Except u) = Except u
    fmap f (Result r) = Result (f r)

-------------------------------------------------------------------------------
-- FIXPOINTS ------------------------------------------------------------------
-------------------------------------------------------------------------------

data Fix f = In ( f ( Fix f ) )

data TreeF a k = LeafF
               | BranchF a k k

instance Functor (TreeF a) where
--  fmap :: (a -> b) -> TreeF a a1 -> TreeF a b1
    fmap f LeafF                  = LeafF
    fmap f (BranchF x left right) = BranchF x (f left) (f right)

cata :: Functor f => (f b -> b) -> Fix f -> b
cata alg (In x) = (alg . fmap ( cata alg ) ) x

sumFix :: Fix (TreeF Int) -> Int
sumFix = cata alg where
    alg :: TreeF Int Int -> Int
    alg LeafF                  = 0
    alg (BranchF x left right) = x + left + right

-- Algebra for counting leaves on TreeF
countFix :: Fix (TreeF Int) -> Int
countFix = cata alg where
    alg :: TreeF Int Int -> Int
    alg LeafF           = 1
    alg (BranchF x l r) = l + r























--end
