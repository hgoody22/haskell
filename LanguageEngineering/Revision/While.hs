module While where
import Yoda

type Var = String

data Aexp
   = Num Int
   | Var Var
   | Aexp :+: Aexp
   | Aexp :*: Aexp
   | Aexp :-: Aexp
   deriving (Show)

data Bexp
   = T
   | F
   | Aexp :=: Aexp
   | Aexp :<=: Aexp
   | Bexp :&&: Bexp
   | Not Bexp
   deriving (Show)

data Stm
   = Var := Aexp
   | Skip
   | Stm :> Stm -- :> is insteand of ;
   | If Bexp Stm Stm
   | While Bexp Stm
   deriving (Show)

while :: Parser Stm
while = stms

aexp :: Parser Aexp
aexp = chainl aexp' (tok "+" *> pure (:+:))
   <|> chainl aexp' ((:*:) <$ tok "*")
   -- <|> chainl aexp' (tok "*" *> pure (:*:))
   -- <|> chainl aexp' (tok "-" *> pure (:-:))

aexp' = Num <$> num
    <|> Var <$> var
    <|> tok "(" *> aexp <* tok ")"

bexp :: Parser Bexp
bexp = undefined

stms :: Parser Stm
stms = chainl stm ((:>) <$ tok ";")

stm :: Parser Stm
stm = (:=) <$> var <* tok ":=" <*> aexp
  <|> Skip <$ tok "skip"
  <|> If <$ tok "if" <*> bexp <* tok "then" <*> stm <* tok "else" <*> stm
  <|> While <$ tok "while" <*> bexp  <* tok "do" <*> stm
  <|> tok "(" *> stms <* tok ")"

chainl p op = p >>= rest where
  rest x = do f <- op
              y <- p
              rest (f x y)
       <|> return x

whitespace :: Parser ()
whitespace = () <$ many (oneOf "/n/t/r ")

num :: Parser Int
num = read <$> some (oneOf ['0' .. '9']) <* whitespace

var :: Parser String
var = some (oneOf ['a' .. 'z']) <* whitespace

tok :: String -> Parser String
tok t = string t <* whitespace























-- end
