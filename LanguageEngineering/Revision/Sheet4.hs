module Sheet4 where

data Fix f = In ( f ( Fix f ) )

cata :: Functor f => (f b -> b) -> Fix f -> b
cata alg (In x) = (alg . fmap ( cata alg ) ) x


data Robot k = Fwd Int k
             | Rt k
             | Lt k
             | Stop
           deriving Show

data Compass = N
             | E
             | S
             | W
             deriving (Eq, Show)

instance Functor Robot where
--  fmap :: (a -> b) -> Robot a -> Robot b
    fmap f (Fwd n x)    = Fwd n (f x)
    fmap f (Rt y)      = Rt (f y)
    fmap f (Lt z)      = Lt (f z)
    fmap f Stop        = Stop

distFix :: Fix Robot -> Int
distFix = cata alg where
    alg :: Robot Int -> Int
    alg (Fwd n m) = n + m
    alg (Rt m)    = m
    alg (Lt m)    = m
    alg Stop      = 0

lt :: Compass -> Compass
lt N = W
lt E = N
lt S = E
lt W = S

rt :: Compass -> Compass
rt N = E
rt E = S
rt S = W
rt W = N

checkDir :: Compass -> Bool
checkDir x | (x == N) || (x == S) = True
           | (x == E) || (x == W) = False

checkDirEast :: Compass -> Bool
checkDirEast x | (x == E) || (x == W) = True
               | (x == N) || (x == S) = False

sameDistFix :: Fix Robot -> Double
sameDistFix = fst . cata alg where
    alg :: Robot (Double, Compass) -> (Double, Compass)
    alg Stop = (0, N)
    alg (Lt (dist, comp)) = (dist, lt comp)
    alg (Rt (dist, comp)) = (dist, rt comp)
    alg (Fwd n (dist, comp)) = if checkDir comp then (dist + fromIntegral(n), comp)
                                                else (dist, comp)

sameDistEastFix :: Fix Robot -> Double
sameDistEastFix = fst . cata alg where
    alg :: Robot (Double, Compass) -> (Double, Compass)
    alg Stop = (0, N)
    alg (Lt (dist, comp)) = (dist, lt comp)
    alg (Rt (dist, comp)) = (dist, rt comp)
    alg (Fwd n (dist, comp)) = if checkDirEast comp then (dist + fromIntegral(n), comp)
                                                    else (dist, comp)


crowDistFix :: Fix Robot -> Double
crowDistFix r = sqrt ( (sameDistFix r)^2 + (sameDistEastFix r)^2 )





















-- end
