module Sheet6 where
import Yoda

-- instance Applicative Maybe where
-- -- pure :: a -> f a
--    pure = Just
-- -- (<*>) :: f (a -> b) -> f a -> f b
--    Just f <*> Just x = Just (f x)
--    _ <*> _ = Nothing

-- CHECK APPLICATIVE LAWS LATER

-------------------------------------------------------------------------------
-- 1.2 Parser Combinators for Lists -------------------------------------------
-------------------------------------------------------------------------------

instance Monoid (List a) where
   mempty = Empty
   mappend Empty y = y
   Cons x xs `mappend` ys = Cons x (mappend xs ys)

data List a = Empty
            | Cons a (List a)
            deriving (Show)

instance Functor List where
-- fmap :: (a -> b) -> f a -> f b
   fmap f Empty       = Empty
   fmap f (Cons a as) = Cons (f a) (fmap f as)

-- (<$) :: a -> List b -> List a
-- x <$ Empty = Cons x Empty
-- x <$ (Cons y ys) = Cons x (x <$ ys)

-- instance Applicative List where
--    pure x = Cons x Empty
--    Empty <*> _ = Empty
--    (Cons f fs) <*> ys = (fmap f ys) `mappend` (fs <*> ys)

-- instance Applicative Maybe where
-- pure = Just
-- Nothing <*> _ = Nothing
-- (Just f) <*> ys = (f <$> ys)

--
-- (<£) :: List a -> List b -> List a
-- Empty <£ _ = Empty
-- (Cons f fs) <£ l = (f <£ l) `mappend` (fs <£ l)
































--end
