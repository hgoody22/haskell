module Uno where

-------------------------------------------------------------------------------
-- Robot ----------------------------------------------------------------------
-------------------------------------------------------------------------------
data Robot = Fwd Int Robot
           | L Robot
           | R Robot
           | Stop
           deriving (Show)

data Compass = N | S | E | W deriving (Show)

distTrav :: Robot -> Int
distTrav (Fwd x r) = x + distTrav r
distTrav (L r)     = distTrav r
distTrav (R r)     = distTrav r
distTrav Stop      = 0

leftTurn :: Compass -> Compass
leftTurn N = W
leftTurn W = S
leftTurn S = E
leftTurn E = N

checkComp :: Compass -> Int -> Int
checkComp N = id
checkComp S = negate
checkComp _ = const 0

distTravFwd :: (Robot, Compass) -> Int
distTravFwd ( (Fwd x r) , c) = checkComp c x + distTravFwd (r, c)
distTravFwd ((L r), c)       = distTravFwd (r, leftTurn c)
distTravFwd ((R r), c)       = distTravFwd (r, leftTurn . leftTurn . leftTurn $ c)
distTravFwd (Stop, c)        = 0

birdsEye :: Robot -> Float
birdsEye r = sqrt ( (fromIntegral (distTravFwd (r, N)))^2 + (fromIntegral(distTravFwd (r, E)))^2 )

-------------------------------------------------------------------------------
-- Cooking Master -------------------------------------------------------------
-------------------------------------------------------------------------------

type Time = Int
type Weight = Int
type Cooked = Bool
type Description = String
type Potato = (Time, Weight, Cooked, Description)

potato :: (Time, Weight, Cooked, Description)
potato = (0, 3, False, "potato")

-- peel
peel :: Potato -> Potato
peel (t, w, c, d) = (t + 2*(w `div` 3), w, c, d ++ " peeled")

-- stew
stew :: Potato -> Potato
stew (t, w, c, d) = (t + 120, w, True, d ++ " stewed")

-- mix
mix :: Potato -> Potato -> Potato
mix (t1, w1, c1, d1) (t2, w2, c2, d2) = (t1 + t2, w1 + w2, c1 && c2, d1 ++ " and " ++ d2)

-------------------------------------------------------------------------------
-- Languages ------------------------------------------------------------------
-------------------------------------------------------------------------------

-- 1.
-- What is the difference between a GPL and a DSL?
-- GPL is turing complete whereas DSL is not necessarily.

-- 2.
-- What programming methods should be used in shallow embeddings
-- and deep embeddings when dealing with dependant interpretation?
-- Shallow : Output is a tuple for the semantic output that evaluates all the functions
--           that need to evaluated and then uses these values as you increment through.
-- Deep : Create new semantic function and call when needed.

-- 3. Comparison
-- Shallow : Easier to create new additional data construct.
number :: Int -> Int
number x = x
-- vs Deep where the data is updated and then every function that uses that
-- data has to be updated too.
data Number = Int

-- Deep : Easier to add on new semantic function.
--        Because shallow holds data in tuple, to add a new data construct
--        all functions have to be updated.











--end
