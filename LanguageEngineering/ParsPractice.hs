module ParsPractice where
import Prelude
import Yoda


whitespace :: Parser ()
whitespace = many (oneOf (" \t\n,.")) *> pure ()

tok :: String -> Parser String
tok x = whitespace *> string x

number :: Parser Int
number = whitespace *> (read <$> (oneOf "123456789") <:> (many (oneOf "0123456789")))

parseRobot :: Parser Robot
parseRobot = Fwd <$ tok "forward" <*> number <*> parseRobot
           <|> L <$ tok "rotate" <* tok "left" <*> parseRobot
           <|> R <$ tok "rotate" <* tok "right" <*> parseRobot
           <|> Stop <$ tok "stop"

robot :: Parser Robot
robot = Fwd <$ tok "forward" <*> number <*> robot
      <|> L <$ tok "rotate" <* tok "left" <*> robot
      <|> R <$ tok "rotate" <* tok "right" <*> robot
      <|> Stop <$ tok "stop"




data Robot = Fwd Int Robot
             | L Robot
             | R Robot
             | Stop
             deriving (Show)




















--end
