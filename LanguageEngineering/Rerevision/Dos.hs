module Dos where

-------------------------------------------------------------------------------
-- Deep -----------------------------------------------------------------------
-------------------------------------------------------------------------------

data Animal = Lion
            | Tiger
            | Gazelle
            | Ant
            deriving (Eq, Show)

type Animals = [Animal]

data Safari = Plot Safari Safari Safari Safari
            | Territory Animals

data Saffy = Plotty [Saffy]
           | Terry Animals

data Core = Split [(Core, Float)]
          | Ter Animals

animals1 :: Animals
animals1 = [Gazelle, Ant]

animals2 :: Animals
animals2 = [Gazelle, Ant, Tiger, Lion]

t1 :: Safari
t1 = Territory animals1

t2 :: Safari
t2 = Territory animals2

t3 :: Saffy
t3 = Terry animals1

t4 :: Saffy
t4 = Terry animals2

t5 :: Core
t5 = Ter animals1

t6 :: Core
t6 = Ter animals2

countTs :: Safari -> Int
countTs (Territory t)  = 1
countTs (Plot a b c d) = countTs a + countTs b + countTs c + countTs d

numberOf :: Saffy -> Int
numberOf (Terry as)  = 1
numberOf (Plotty xs) = sum (map numberOf xs)

modalT :: Saffy -> Int
modalT (Terry as)  = length as
modalT (Plotty xs) = maximum (map modalT xs)

totalZoo :: Saffy -> Animals
totalZoo (Terry as)  = as
totalZoo (Plotty xs) = concat (map totalZoo xs)

numberCores :: Core -> Int
numberCores (Ter x)    = 1
numberCores (Split xs) = sum (map (numberCores . fst) xs)

modalCore :: Core -> Int
modalCore (Ter x)    = length x
modalCore (Split xs) = maximum (map (modalCore . fst) xs)

totalCore :: Core -> Animals
totalCore (Ter x)    = x
totalCore (Split xs) = concat (map (totalCore . fst) xs)

-------------------------------------------------------------------------------
-- Shallow --------------------------------------------------------------------
-------------------------------------------------------------------------------

type ShallowV = (Int, Int, Animals)

territory :: Animals -> ShallowV
territory xs = (1, length xs, xs)

quadrant :: ShallowV -> ShallowV -> ShallowV -> ShallowV -> ShallowV
quadrant (n1, m1, a1) (n2, m2, a2) (n3, m3, a3) (n4, m4, a4) = (sum [n1, n2, n3, n4], maximum [m1, m2, m3, m4], concat [a1, a2, a3, a4])

shallow :: ShallowV
shallow = (2, 4, [Tiger, Lion, Ant, Gazelle])

getAnimals :: ShallowV -> Animals
getAnimals (x, y, z) = z

core :: [ShallowV] -> Animals
core xs = concat (map getAnimals xs)

-- Using Animal not string is important as it ensures that no junk enters the system
-- "abc" could be a string but not an animal

-- Embedded
quad :: Core -> Core -> Core -> Core -> Core
quad a b c d = Split [(a, 1/4), (b, 1/4), (c, 1/4), (d, 1/4)]

-- 1. Advantage of shallow over deep is that shallow allows extention of a new
--    data construct in embedding without changing any existing code

-- 2. Advantage of deep over shallow is that deep embedding allows extention of
--    new semantic function without changing code

-- 3. Combination:
--    Deep core embedding makes designing the language easier.
--    Semantic functions can therefore be added easily.
--    Core data type is difficult for a person to use so smart constructors,
--    created with shallow embedding, map the core data type as the carrier
--    and so allows you to create new data constructors without changing previous
--    code.   


























-- end
