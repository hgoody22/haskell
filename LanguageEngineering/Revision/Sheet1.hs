module Sheet1 where


-------------------------------------------------------------------------------
-- Deep embedding -------------------------------------------------------------
-------------------------------------------------------------------------------

data Robot = Fwd Int Robot
           | Rt Robot
           | Lt Robot
           | Stop
           deriving Show

data Compass = N
             | E
             | S
             | W

distTrav :: Robot -> Int
distTrav (Fwd x r) = x + distTrav r
distTrav (Rt r)    = distTrav r
distTrav (Lt r)    = distTrav r
distTrav Stop      = 0

lt :: Compass -> Compass
lt N = W
lt E = N
lt S = E
lt W = S

rt :: Compass -> Compass
rt N = E
rt E = S
rt S = W
rt W = N

distSame :: Robot -> Compass -> Int
distSame (Fwd x r) N = x + distSame r N
distSame (Fwd x r) S = (-x) + distSame r S
distSame (Fwd x r) E = distSame r E
distSame (Fwd x r) W = distSame r W
distSame (Rt r) c    = distSame r (rt c)
distSame (Lt r) c    = distSame r (lt c)
distSame Stop c      = 0

-------------------------------------------------------------------------------
-- Shallow embedding -------------------------------------------------------------
-------------------------------------------------------------------------------

type Time = Int
type Weight = Int
type Cooked = Bool
type Description = String

potato :: (Time, Weight, Cooked, Description)
potato = (0, 3, False, "potato")

peel :: (Time, Weight, Cooked, Description) -> (Time, Weight, Cooked, Description)
peel (a, b, c, d) = (a + 2, b, c, d ++ " peeled")

roast :: (Time, Weight, Cooked, Description) -> (Time, Weight, Cooked, Description)
roast (a, b, c, d) = (a + 70, b, True, d ++ " roasted")

boil :: (Time, Weight, Cooked, Description) -> (Time, Weight, Cooked, Description)
boil (a, b, c, d) = (a + 25, b, c, d ++ " mashed")

mix :: (Time, Weight, Cooked, Description) -> (Time, Weight, Cooked, Description) -> (Time, Weight, Cooked, Description)
mix (a1, b1, c1, d1) (a2, b2, c2, d2) = (a1 + a2, b1 + b2, c1 && c2, d1 ++ " " ++ d2)























--end
