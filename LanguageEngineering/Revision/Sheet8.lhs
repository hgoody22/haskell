>module Sheet8 where

-----------------------------------------------------------------------------
-- Monad Laws ---------------------------------------------------------------
-----------------------------------------------------------------------------

-- Left return   : return x >>= f      = f x
-- Right return  : m x >>= return      = m x
-- Associativity : ( m x >>= f ) >>= g = m x >>= ( \x -> f x >>= g )

-- fake f
>f :: Num a => a -> Maybe a
>f x = (Just (x+1))

-- Left return
>proof1 :: a -> (a -> Maybe b) -> Maybe b
>proof1 m f = (return m >>= f)
-- {def return} Just m >>= f
-- {def >>=} f m :: Maybe b

-- Right return
>proof2 :: Maybe a -> Maybe a
>proof2 Nothing = (Nothing >>= return)
-- {def >>=} Nothing :: Maybe a
>proof2 (Just x) = (Just x >>= return)
-- {def >>=} return x
-- {def return} Just x :: Maybe a

-- Associativity
>proof3 :: Maybe a -> (a -> Maybe b) -> (b -> Maybe c) -> Maybe c
>proof3 Nothing f g = (Nothing >>= f) >>= g
-- {def >>=} Nothing >>= g
-- {def >>=} Nothing
-- {def >>=} Nothing >>= (\x -> f x >>= g) :: Maybe c
>proof3 (Just x) f g = (Just x >>= (\y -> f y >>= g))
-- {def >>=} (\y -> f y >>= g) x
-- {def \} f x >>= g
-- {def >>=} ((Just x) >>= f) >>= g

-- Show Monad subset of Functor
-- fmap f x = x >>= return . f

-- instance Monad Maybe where
--   --return :: a -> Mayb a
--   return = Just
--   --(>>=) :: Mayb a -> (a -> Mayb b) -> Mayb b
--   (Just x) >>= f = f x
--   Nothing >>= f  = Nothing

-- Monad instance practice
data Exception' e a = Throw' e
                    | Continue' a

instance Monad (Exeception e) where
-- return :: a -> Exeception e a
   return = Continue
-- (>>=) :: Exception e a -> (a -> Exception e b) -> Exception e b
   (Throw e)    >>= f = Throw e
   (Continue a) >>= f = f a
































--end
