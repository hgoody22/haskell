module Sheet8 where
import Data.Monoid
import Data.Functor
import Control.Applicative

-----------------------------------------------------------------------------
-- Monad Laws ---------------------------------------------------------------
-----------------------------------------------------------------------------

-- Left return   : return x >>= f      = f x
-- Right return  : m x >>= return      = m x
-- Associativity : ( m x >>= f ) >>= g = m x >>= ( \x -> f x >>= g )

-- fake f
f :: Num a => a -> Maybe a
f x = (Just (x+1))

-- Left return
proof1 :: a -> (a -> Maybe b) -> Maybe b
proof1 m f = (return m >>= f)
-- {def return} Just m >>= f
-- {def >>=} f m :: Maybe b

-- Right return
proof2 :: Maybe a -> Maybe a
proof2 Nothing = (Nothing >>= return)
-- {def >>=} Nothing :: Maybe a
proof2 (Just x) = (Just x >>= return)
-- {def >>=} return x
-- {def return} Just x :: Maybe a

-- Associativity
proof3 :: Maybe a -> (a -> Maybe b) -> (b -> Maybe c) -> Maybe c
proof3 Nothing f g = (Nothing >>= f) >>= g
-- {def >>=} Nothing >>= g
-- {def >>=} Nothing
-- {def >>=} Nothing >>= (\x -> f x >>= g) :: Maybe c
proof3 (Just x) f g = (Just x >>= (\y -> f y >>= g))
-- {def >>=} (\y -> f y >>= g) x
-- {def \} f x >>= g
-- {def >>=} ((Just x) >>= f) >>= g

-- Show Monad subset of Functor
-- fmap f x = x >>= return . f

-- instance Monad Maybe where
--   --return :: a -> Mayb a
--   return = Just
--   --(>>=) :: Mayb a -> (a -> Mayb b) -> Mayb b
--   (Just x) >>= f = f x
--   Nothing >>= f  = Nothing

-- Monad instance practice

-- Exception e a = Throw e
              -- | Continue a

-- instance Monad (Exception e) where
-- return :: a -> Exception e a
   -- return = Continue
-- (>>=) :: Exception e a -> (a -> Exception e b) -> Exception e b
   -- (Throw e)    >>= f = Throw e
   -- (Continue a) >>= f = f a

-------------------------------------------------------------------------------
--Tree Monad -------------------------------------------------------------------
-------------------------------------------------------------------------------
data Tree a = Leaf a
            | Fork (Tree a) (Tree a)
            deriving (Show, Eq)

instance Functor Tree where
-- fmap :: (a -> b) -> Tree a -> Tree b
   fmap f (Leaf x) = Leaf (f x)
   fmap f (Fork left right) = Fork (fmap f left) (fmap f right)

instance Applicative Tree where
-- pure :: a -> Tree a
   pure x = Leaf x
--(<*>) :: Tree (a -> b) -> Tree a -> Tree b
   (Leaf f) <*> (Leaf x) = Leaf (f x)
   (Leaf f) <*> (Fork left right) = Fork (f <$> left) (f <$> right)
   (Fork l r)   <*> tree = Fork (l <*> tree) (r <*> tree)

instance Monad Tree where
-- return :: a -> Tree a
   return x = Leaf x
-- (>>=) :: Tree a -> (a -> Tree b) -> Tree b
   (Leaf x) >>= f = f x
   (Fork left right) >>= f = Fork (left >>= f) (right >>= f)

-- Left return : return x >>= f = f x
proof4 :: a -> (a -> Tree b) -> Tree b
proof4 x f = return x >>= f
-- {def return} Leaf x >>= f
-- {def >>=}    f x

-- Right return : m x >>= return = m x
proof5 :: Tree a -> Tree a
proof5 (Leaf x) = (Leaf x) >>= return
-- {def >>=}    return x
-- {def return} Leaf x
proof5 (Fork left right) = (Fork left right) >>= return
-- {def >>=} Fork (left >>= return) (right >>= return)
-- {induction hypothesis: forall t in Tree, t >>= return = t} Fork left right

proof6 :: Tree a -> (a -> Tree b) -> (b -> Tree c) -> Tree c
proof6 (Leaf x) f g = Leaf x >>= (\y -> f y >>= g)
-- {def >>=}      (\y -> f y >>= g) x
-- {def unwrap \} f x >>= g
-- {def >>=}      (Leaf x >>= f) >>= g
proof6 (Fork left right) f g = Fork left right >>= (\y -> f y >>= g)
-- {def >>=}      Fork (left >>= (\y -> f y >>= g)) (right >>= (\y -> f y >>= g))
-- {def ind hyp:} Fork (left >>= f >>= g) (right >>= f >>= g)
-- {def >>=}      Fork (left >>= f) (right >>= f) >>= g
-- {def >>=}      ((Fork left right) >>= f) >>= g

intToDoubleTree :: Int -> Tree Int
intToDoubleTree x = return (2 * x)

addToTree :: Int -> Int -> Tree Int
addToTree x y = return (x + y)

manipTree :: Tree Int -> Tree Int
manipTree tx   = tx >>= addToTree 3 >>= intToDoubleTree >>= addToTree 2
-- manipTree (Fork l r) = Fork (manipTree l) (manipTree r)

dupToTree :: a -> Tree a
dupToTree x = Fork (return x) (return x)

dupWholeTree :: Tree a -> Tree a
dupWholeTree tx = tx >>= dupToTree

doubleEverything :: Tree Int -> Tree Int
doubleEverything tx = tx >>= intToDoubleTree >>= dupToTree

doubleEverything' :: Tree Int -> Tree Int
doubleEverything' tx = tx >>= dupToTree >>= intToDoubleTree 














--end
