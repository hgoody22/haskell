>module MonadPractice where
>import Prelude

putChar :: Char -> IO () == Prints Char

putStrLn :: String -> IO () == Prints string + newline
putStrLn xs = foldr (>>) done (MAP putChar xs) >> putChar '\n'

done :: IO () == This does nothing

(>>) :: IO a -> IO b -> IO a == Gives order to commands, eg p >> q means do action p then do action q

getChar :: IO Char == Reads character from keyboard

return :: a -> IO a == Does nothing and returns a
                       Think done = return ()
























--end
